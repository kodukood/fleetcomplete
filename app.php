<?php

class FleetAPI {

	private $key;
	private $endPoint;

	public function __construct( $key, $endPoint ) {
		$this->key = $key;
		$this->endPoint = $endPoint;
	}

	// Method: POST, PUT, GET etc
	// Data: array("param" => "value") ==> index.php?param=value
	private function call( $method, $url, $data = false ) {

	    $curl = curl_init();

	    switch ($method)
	    {
	        case "POST":
	            curl_setopt($curl, CURLOPT_POST, 1);

	            if ($data)
	                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
	            break;
	        case "PUT":
	            curl_setopt($curl, CURLOPT_PUT, 1);
	            break;
	        default:
	            if ($data)
	                $url = sprintf("%s?%s", $url, http_build_query($data));
	    }

	    // Optional Authentication:
	    //curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	    //curl_setopt($curl, CURLOPT_USERPWD, "username:password");

	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

	    $result = curl_exec($curl);

	    curl_close($curl);

	    return $result;
	}

	public function getLastData() {
		return json_decode( $this->call('GET', $this->endPoint . __FUNCTION__, array('key' => $this->key, 'json' => 1 ) ) );
	}

	public function getRawData( $objectId, $beginDate, $endDate ) {

		return json_decode( $this->call('GET', $this->endPoint . __FUNCTION__, array(
			'objectId' => $objectId,
			'begTimestamp' => $beginDate,
			'endTimestamp' => $endDate,
			'key' => $this->key, 
			'json' => 1
		)));	
	}
}

class Controller {

	private $context;

	function __construct() {
		$this->context = new Context();
	}

	function getContext() {
		return $this->context;
	}

	function process() {

		$cmd = CommandFactory::getCommand( $this->context->get('action') );

		if ( !$cmd->execute( $this->context ) ) {
			// handle failure
		} else {
			// all is well;
		}
	}
}

class Context {

	private $params = array();
	private $error = "";

	function __construct() {
		$this->params = $_REQUEST;
	}

	function addParam( $key, $val ) {
		$this->params[$key] = $val;
	}

	function get( $key ) {
		return $this->params[$key];
	}

	function setError( $error ) {
		$this->error = $error;
	}

	function getError() {
		return $this->error;
	}
}

class CommandFactory {

	static function getCommand( $action ) {
		$class = ucfirst(strtolower($action)) . "Command";

		if ( !class_exists($class) ) {
			throw new Exception( "class " . $class . " not defined" );
		}

		$cmd = new $class();
		return $cmd;
	}
}

abstract class Command {
	abstract function execute( Context $context );
}

abstract class StopsFinder {
	
	protected $vehicleData;
	protected $context;

	public function __construct( $vehicleData, Context $context ) {
		$this->vehicleData = $vehicleData;
		$this->context = $context;		
	}

	public static function getInstance( $vehicleData, Context $context ) {

		if ( property_exists($vehicleData[0], 'Distance') ) {

			return new DistanceStopsFinder( $vehicleData, $context );

		} elseif ( property_exists($vehicleData[0], 'Speed') ) {

			return new SpeedStopsFinder( $vehicleData, $context );

		} else {

			throw new Exception( "Unsupported property" );
		}
	}

	// get stops' coordinates
	public function getStops() {

		$stops = array();

		$k = 1;
		for ($i = 0; $i < count( $this->vehicleData ); $i++) {
			if ( $i == 0 ) {
				$stops[] = (object) array(
					//'id'	=> $i,
					'lat'	=> $this->vehicleData[ $i ]->Latitude,
					'lng'	=> $this->vehicleData[ $i ]->Longitude
				);
				$state = 'stop';
			} elseif ($state == 'stop' && $this->changeDetected( $i )) {
				$state = 'move';
			} elseif ( $state == 'move' ) {
				if ( $this->changeDetected( $i )) {
					$k = 1;
				} else {
					if ( (new DateTime($this->vehicleData[ $i ]->timestamp))->getTimestamp()
							- (new DateTime($this->vehicleData[ $i - $k ]->timestamp))->getTimestamp() > $this->context->get('stopLength') ) {
						$stops[] = (object) array(
							//'id'	=> $i - $k,
							'lat'	=> $this->vehicleData[ $i - $k ]->Latitude,
							'lng'	=> $this->vehicleData[ $i - $k ]->Longitude
						);
						$state = 'stop';
						$k = 1;
					} else {
						$k++;
					}
				}
			}
		}

		// get the last stop
		if ( $state == 'move' ) {
			$stops[] = (object) array(
				//'id'	=> count($this->vehicleData) - $k,
				'lat'	=> $this->vehicleData[ count($this->vehicleData) - $k ]->Latitude,
				'lng'	=> $this->vehicleData[ count($this->vehicleData) - $k ]->Longitude
			);
		}

		return $stops;
	}

	abstract protected function changeDetected( $i );	
}

class DistanceStopsFinder extends StopsFinder {

	protected function changeDetected( $i ) {
		return ($this->vehicleData[ $i ]->Distance != $this->vehicleData[ $i - 1 ]->Distance);
	}

}

class SpeedStopsFinder extends StopsFinder {

	protected function changeDetected( $i ) {

		return ($this->vehicleData[ $i ]->Speed > 0);
	}

}

class LocationCommand extends Command {

	function execute( Context $context ) {
		//echo json_encode("Executing " . get_class($this));

		$api = new FleetAPI( $context->get('key'), $context->get('endPoint') );
		$data = $api->getLastData();

		$output = new stdClass;
		$output->vehicles = array();
		$output->locations = array();

		foreach ( $data->response as $vehicleData ) {

			$output->vehicles[] = (object) array(
				'id'		=> $vehicleData->objectId,
				'name'		=> $vehicleData->plate,
				'speed'		=> str_replace("null", "", $vehicleData->speed),
				'timestamp'	=> (new DateTime($vehicleData->timestamp))->format('d.m.Y H:i:s')
			);

			$output->locations[] = (object) array(
				'name'		=> $vehicleData->plate,
				'latitude'	=> $vehicleData->latitude,
				'longitude'	=> $vehicleData->longitude
			);
		}

		echo json_encode($output);

	}
}

class RouteCommand extends Command {

	function execute( Context $context ) {

		$api = new FleetAPI( $context->get('key'), $context->get('endPoint') );

		$startDate = DateTime::createFromFormat('d/m/Y', $context->get('date') );

		$endDate = clone $startDate;
		$endDate->add(new DateInterval('P1D'));


		$data = $api->getRawData( $context->get('id'), $startDate->format('Y-m-d') , $endDate->format('Y-m-d') );


		$output = new stdClass;
		$output->coordinates = array();
		$output->distance = 0;
		$output->stops = array();

		// get coordinates for plotting the route
		for ($i = 0; $i < count($data->response); $i++) {

			if ( $i > 0 && $data->response[ $i ]->Latitude == $data->response[ $i - 1 ]->Latitude && $data->response[ $i ]->Longitude == $data->response[ $i - 1 ]->Longitude ) {
				continue;
			}

			$output->coordinates[] = (object) array(
				'lat'	=> $data->response[ $i ]->Latitude,
				'lng'	=> $data->response[ $i ]->Longitude
			);	
		}

		if ( is_array( $data->response ) && count($data->response) ) {

			// get total distance
			$output->distance = round($data->response[ count($data->response) - 1 ]->Distance - $data->response[ 0 ]->Distance, 2);

			// get stops
			try {
				$stopsFinder = StopsFinder::getInstance( $data->response, $context );
				$output->stops = $stopsFinder->getStops();
			} catch (Exception $e) {
			    //echo 'Caught exception: ',  $e->getMessage(), "\n";
			}

		}

		echo json_encode($output);
	}
}

$controller = new Controller();

// set API parameters
$context = $controller->getContext();
$context->addParam( 'endPoint', 'https://app.ecofleet.com/seeme/Api/Vehicles/' ); // API endpoint
$context->addParam( 'stopLength', 300 ); // stop length in seconds

//$context->addParam( 'action', 'route' );
//$context->addParam( 'id', '187360' );
//$context->addParam( 'date', '22/04/2021' );


$controller->process();



?>