$( function() {
$( "#datepicker" ).datepicker({
  showOn: "button",
  buttonImage: "images/datepicker.png",
  buttonImageOnly: true,
  buttonText: "Select date",
  dateFormat: "dd/mm/yy"
});
} );


/* All map related functions go here */
let ShowMap = {

	map: {},
	markers: [],
	routes: [],
	directionsServiceResult: {},
	directionsRenderer: {},

    init: function() {
		let tartu = { lat: 58.372154772144285, lng: 26.732109076872202 };

		// The map, centered at Tartu
		ShowMap.map = new google.maps.Map(document.getElementById("map"), {
			zoom: 14,
			center: tartu,
		});
    },

	setMarkers: function( locations ) {

		locations.forEach(function(location) {

			let myLatLng = new google.maps.LatLng( location.latitude, location.longitude );

	        let marker = new google.maps.Marker({
	            position: myLatLng,
	            map: ShowMap.map,
	            animation: google.maps.Animation.DROP,
	            title: location.name
	        });

			ShowMap.markers.push(marker);

		});

		let bounds  = new google.maps.LatLngBounds();

	    for (let i=0; i<ShowMap.markers.length; i++) {
	    	bounds.extend(ShowMap.markers[i].getPosition());
	    }

		ShowMap.map.setCenter(bounds.getCenter());
		ShowMap.map.fitBounds(bounds);
	},

	reloadMarkers: function( locations ) {

		ShowMap.clearMap();

		// Call set markers to re-add markers
		ShowMap.setMarkers(locations);
	},

	removeMarkers: function() {

	    // Loop through markers and set map to null for each
	    for (let i=0; i<ShowMap.markers.length; i++) {
	        ShowMap.markers[i].setMap(null);
	    }
	    
	    // Reset the markers array
	    ShowMap.markers = [];
	},

	removeRoutes: function() {

	    // Loop through markers and set map to null for each
	    for (let i=0; i<ShowMap.routes.length; i++) {
	        ShowMap.routes[i].setMap(null);
	    }

	    // Reset the markers array
	    ShowMap.routes = [];
	},

	removeShortestRoute: function() {

		if (ShowMap.directionsRenderer && typeof ShowMap.directionsRenderer === 'object' && Object.keys(ShowMap.directionsRenderer).length != 0) {
			ShowMap.directionsRenderer.setMap(null);
			ShowMap.directionsRenderer = null;
		}
	},

	clearMap: function() {
		ShowMap.removeMarkers();
		ShowMap.removeRoutes();
		ShowMap.removeShortestRoute();
	},

	displayRoute: function( coordinates ) {

		ShowMap.clearMap();

		route = new google.maps.Polyline({
			path: coordinates,
			strokeColor: "#FF0000",
			strokeOpacity: 1.0,
			strokeWeight: 2,
		});

		route.setMap(ShowMap.map);

		ShowMap.routes.push(route);


		let bounds  = new google.maps.LatLngBounds();

	    for (let i=0; i<coordinates.length; i++) {
	    	bounds.extend(coordinates[i]);
	    }

		ShowMap.map.setCenter(bounds.getCenter());
		ShowMap.map.fitBounds(bounds);
	},

	calculateShortestDistance: function( stops ) {

		let directionsService = new google.maps.DirectionsService();

		let waypts = [];

		let orig = stops.shift();
		let dest = stops.pop();

		for (let i = 0; i < stops.length; i++) {
			waypts.push({
				location: stops[i],
				stopover: true,
			});
		}

		directionsService.route(
		{
			origin: orig,
			destination: dest,
			waypoints: waypts,
			optimizeWaypoints: false,
			travelMode: google.maps.TravelMode.DRIVING,
			unitSystem: google.maps.UnitSystem.METRIC,
		},
		(response, status) => {
			if (status === "OK" && response) {
				console.log("Directions request successful");
				ShowMap.directionsServiceResult = response;

				let markup = '<tr><td>Shortest possible distance</td><td>' + ShowMap.computeTotalDistance(response) + ' km</td></tr>';
				$("#vehicledata > tbody").append(markup);

				let link = $('<a>',{
				    text: 'display',
				    title: 'Display the shortest route',
				    href: '#',
				    click: function(){ ShowMap.displayShortestRoute();return false;}
				});

				$("#vehicledata > tbody tr").last().find('td:first').append(' (', link, ')');

			} else {
				console.log("Directions request failed due to " + status);
			}
		}
		);
	},

	computeTotalDistance: function( result ) {

		let total = 0;
		const myroute = result.routes[0];

		if (!myroute) {
			return;
		}

		for (let i = 0; i < myroute.legs.length; i++) {
			total += myroute.legs[i].distance.value;
		}
		total = total / 1000;

		return parseFloat(total).toFixed(2);
	},

	displayShortestRoute: function() {

		ShowMap.directionsRenderer = new google.maps.DirectionsRenderer();
		ShowMap.directionsRenderer.setMap(ShowMap.map);
		ShowMap.directionsRenderer.setDirections(ShowMap.directionsServiceResult);

	},

	calculateDistance: function( coordinates ) {

		let distance = 0;

		for (let i = 0; i < coordinates.length; i++) {

			let k = i + 1;

			if ( coordinates[k] ) {
				distance += google.maps.geometry.spherical.computeDistanceBetween(
				    new google.maps.LatLng({
				        lat: coordinates[i].lat, 
				        lng: coordinates[i].lng
				    }),
				    new google.maps.LatLng({
				        lat: coordinates[k].lat, 
				        lng: coordinates[k].lng
				    })
				);

			}
		}

		return parseFloat((distance * 0.001)).toFixed(2);
	}
}

window.initMap = function() {
   window.initMap = null; //set this to null this so that it can't get called anymore
   ShowMap.init();
};


$(document).ready(function(){

	$('#submitKey').click(function(){

		let params = {
		  action: "location",
		  key: $( "#apikey" ).val()
		};

		$('#submitDate').data( "key", $( "#apikey" ).val() );

		$("#vehicles > tbody").empty();
		$("#vehicledata > tbody").empty();

	    $.ajax({
	      url: 'app.php',
	      type: 'POST',
	      dataType: "json",
	      data: params	      
	    })
	    .done(function(data){

			$.each( data.vehicles, function( i, cardata ) {

				let markup = '<tr><td>' + cardata.name + '</td><td>' + cardata.speed + '</td><td>' + cardata.timestamp + '</td></tr>';
				$("#vehicles > tbody").append(markup);

				$("#vehicles > tbody tr").last().data( "id", cardata.id );

			});

			ShowMap.reloadMarkers( data.locations );

			$("#vehicles > tbody tr").click(function(){

				$(this).addClass('selected').siblings().removeClass('selected');

				console.log( 'Selected id: ' + $(this).data( "id" ) );

				$('#submitDate').data( "id", $(this).data( "id" ) );

			});
	    })
	    .fail(function() {
	    	console.log('Failed to receive data');
	    });
	});


	$('#submitDate').click(function(){

		let params = {
		  action: "route",
		  key: $(this).data( "key" ),
		  id: $(this).data( "id" ),
		  date: $( "#datepicker" ).val()
		};

		$("#vehicledata > tbody").empty();

	    $.ajax({
	      url: 'app.php',
	      type: 'POST',
	      dataType: "json",
	      data: params	      
	    })
	    .done(function(data){

			console.log('Received data');

			if (data.coordinates.length > 0) {
				ShowMap.displayRoute( data.coordinates );

				let distance = data.distance;

				if ( distance == 0 && data.coordinates.length > 1) {
					distance = ShowMap.calculateDistance( data.coordinates );
				}

				let markup = '<tr><td>Total distance</td><td>' + distance + ' km</td></tr><tr><td>Number of stops</td><td>' + data.stops.length + '</td></tr>';
				$("#vehicledata > tbody").append(markup);

			} else {
				console.log( 'Received data, but no coordinates' );
			}

			ShowMap.removeShortestRoute();
			if (data.stops.length > 1) {
				ShowMap.calculateShortestDistance(data.stops);
			} else {
				console.log( 'Less than two stops, cant calculate shortest distance' );
			}

	    })
	    .fail(function() {
	    	console.log('Failed to receive data');
	    });
	});

});